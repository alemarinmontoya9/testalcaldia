import axios from 'axios';

export class ListService {
    baseUrl = "http://localhost:8096/list";

    getAll(){
        return axios.get(this.baseUrl ).then(res => res.data);
    }

    save(list) {
        return axios.post(this.baseUrl , list).then(res => res.data);
    }

    delete(list) {
        return axios.get(this.baseUrl , list).then(res => res.data);
    }
}
///limberth770@gmail.com