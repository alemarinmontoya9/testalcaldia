import React, { Component } from 'react';
import './App.css';
import { ListService } from './service/ListService';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import {Panel} from 'primereact/panel';
import {Menubar} from 'primereact/menubar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Growl} from 'primereact/growl';
import { InputTextarea } from 'primereact/inputtextarea';


import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

export default class App extends Component{
  constructor(){
    super();
    this.state = {
      visible : false,
      list: {
        id: null,
        nombre: null,
        descripcion: null,
        fecha_registro: null
      },
      selectedList : {

      }
    };
    this.items = [
      {
        label : 'Nuevo',
        icon  : 'pi pi-fw pi-plus',
        command : () => {this.showSaveDialog()}
      },
      {
        label : 'Editar',
        icon  : 'pi pi-fw pi-pencil',
        command : () => {this.showEditDialog()}
      },
      {
        label : 'Eliminar',
        icon  : 'pi pi-fw pi-trash',
        command : () => {this.delete()}
      }
    ];
    this.listService = new ListService();
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this.footer = (
      <div>
        <Button label="Guardar" icon="pi pi-check" onClick={this.save} />
      </div>
    );
  }

  componentDidMount(){
    this.listService.getAll().then(data => this.setState({lists: data}))
  }

  save() {
    this.listService.save(this.state.list).then(data => {
      this.setState({
        visible : false,
        list: {
          id: null,
          nombre: null,
          descripcion: null,
          fecha_registro: null
        }
      });
      this.growl.show({severity: 'success', summary: 'Atencion!', detail: 'Se guardo el registro correctamente.'});
      this.listService.getAll().then(data => this.setState({lists: data}))
    })
  }

  delete() {
    if(window.confirm("¿Seguro que quiere eliminar el registro?")) {
      this.listService.delete(this.state.selectedList.id).then(data => {
        this.growl.show({severity: 'success', summary: 'Atención!', detail: 'Se elimino el registro correctamente.'});
        this.listService.getAll().then(data => this.setState({lists: data}));
      });
    }
  }

  render(){
    return (
      <div style={{width:'80%', margin: '0 auto', marginTop: '20px'}}>
        <Menubar model={this.items}/>
        <br/>
        <Panel header="React CRUD List">
            <DataTable value={this.state.lists}   selectionMode="single" selection={this.state.selectedList} onSelectionChange={e => this.setState({selectedList: e.value})}>
              <Column field="id" header="Id"></Column>
              <Column field="nombre" header="Nombre"></Column>
              <Column field="descripcion" header="Descripcion"></Column>
              <Column field="fecha_registro" header="Fecha Registro"></Column>
              
            </DataTable>
        </Panel>
        <Dialog header="Crear Lista" visible={this.state.visible} style={{width: '400px'}} footer={this.footer} modal={true} onHide={() => this.setState({visible: false})}>
            <form id="list-form">
              <span className="p-float-label">
                <InputText value={this.state.list.nombre} style={{width : '100%'}} id="nombre" onChange={(e) => {
                    let val = e.target.value;
                    this.setState(prevState => {
                        let list = Object.assign({}, prevState.list);
                        list.nombre = val;

                        return { list };
                    })}
                  } />
                <label htmlFor="nombre">Nombre</label>
              </span>
              <br/>
              <span className="p-float-label">
                <InputTextarea value={this.state.list.descripcion} style={{width : '100%'}} id="descripcion" onChange={(e) => {
                    let val = e.target.value;
                    this.setState(prevState => {
                        let list = Object.assign({}, prevState.list);
                        list.descripcion = val

                        return { list };
                    })}
                  } />
                <label htmlFor="descripcion">Descripcion</label>
              </span>
              <br/>
              
              <br/>
            </form>
        </Dialog>
        <Growl ref={(el) => this.growl = el} />
      </div>
    );
  }

  showSaveDialog(){
    this.setState({
      visible : true,
      list : {
        id: null,
        nombre: null,
        descripcion: null,
        fecha_registro: null
      }
    });
    document.getElementById('list-form').reset();
  }

  showEditDialog() {
    this.setState({
      visible : true,
      list : {
        id: this.state.selectedList.id,
        nombre: this.state.selectedList.nombre,
        descripcion: this.state.selectedList.descripcion,
        fecha_registro: this.state.selectedList.fecha_registro
      }
    })
  }
}
