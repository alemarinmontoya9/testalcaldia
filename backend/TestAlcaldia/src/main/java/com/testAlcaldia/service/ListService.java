
package com.testAlcaldia.service;

import com.testAlcaldia.entity.ListEntity;
import com.testAlcaldia.repo.ListRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListService {
    
    @Autowired
    private ListRepo listRepo;
    
    public ListEntity insertar(ListEntity list){
        return listRepo.save(list);
    }
    
    public ListEntity actualizar(ListEntity list){
        return listRepo.save(list);
    }
    
    public List<ListEntity> listar(){
        return listRepo.findAll();
    }
    
    public void eliminar(ListEntity list){
        listRepo.delete(list);
    }
    
}
