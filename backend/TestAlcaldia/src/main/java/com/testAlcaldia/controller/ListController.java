
package com.testAlcaldia.controller;

import com.testAlcaldia.entity.ListEntity;
import com.testAlcaldia.service.ListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/list")

@CrossOrigin("*")
public class ListController {
    
    @Autowired
    private ListService listService;
    
    @GetMapping
    public List<ListEntity> listar(){
        return listService.listar();
    }
    
    @PostMapping
    public ListEntity insertar(@RequestBody ListEntity list){
        return listService.insertar(list);
    }
    
    @PutMapping
    public ListEntity actualizar(@RequestBody ListEntity list){
        return listService.actualizar(list);
    }

    @DeleteMapping
    public void eliminar(@RequestBody ListEntity list){
        listService.eliminar(list);
    }
    
}
