
package com.testAlcaldia.repo;

import com.testAlcaldia.entity.ListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ListRepo extends JpaRepository<ListEntity, Integer>{
    
}
